package keepass;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

public class JPF_com_keepassdroid_crypto_NativeAESCipherSpi extends NativePeer {
  @MJI
  public static void nCleanup(MJIEnv env, int clsRef, long ctxPtr) {

  }

  @MJI
  public int nFinal(MJIEnv env, int clsRef, long ctxPtr, boolean usePadding, int output,
                     int outputOffest, int outputSize) {
    return outputSize;

  }

  @MJI
  public int nGetCacheSize(MJIEnv env, int clsRef, long ctxPtr) {
    return 256;

  }

  @MJI
  public long nInit(MJIEnv env, int clsRef, boolean encrypting, int key, int iv) {
    return 0x100;

  }

  @MJI
  public int nUpdate(MJIEnv env, int clsRef, long ctxPtr, int input, int inputOffset, int inputLen,
                      int output, int outputOffset, int outputSize) {
    return outputSize;

  }
}
