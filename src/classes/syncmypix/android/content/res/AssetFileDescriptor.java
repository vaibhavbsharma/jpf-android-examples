package android.content.res;

import android.os.Parcel;

public class AssetFileDescriptor implements android.os.Parcelable {
  public static android.content.res.AssetFileDescriptor TOP = new android.content.res.AssetFileDescriptor();


  public AssetFileDescriptor(){
  }

  public java.io.FileOutputStream createOutputStream() throws java.io.IOException {
    return null;
  }

  public void close() throws java.io.IOException {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {

  }
}