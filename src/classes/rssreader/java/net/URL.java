package java.net;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public final class URL implements Serializable {

  @FilterField
  String spec;

  static {
    setURLContents("http://feeds.feedburner.com/Mobilecrunch.rss", "input.rss");
  }

  public URL(String spec) throws MalformedURLException {
    // to be intercepted
  }

  protected static native void setURLContents(String spec, String filename);

  public InputStream openStream() throws IOException {
    boolean b = AndroidVerify.getBoolean("URL.openStream() - success?");

    if (!b) {
      throw new IOException("Could not open Url");
    } else {
      return new ByteArrayInputStream(getURLInput());
    }

  }

  protected native byte[] getURLInput();

}
