//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
//
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
//
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//

package gov.nasa.jpf.util.event.events;

import java.util.Arrays;

import android.view.KeyEvent;

public class KeyPressEvent extends EventImpl {
  protected String target; // the component id (enumeration number)
  protected String action; // the method name

  protected Object[] arguments;

  private KeyEvent keyEvent = new KeyEvent(1, 0, KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_BACK, 1, 0, 0, 0, KeyEvent.FLAG_LONG_PRESS);

  public KeyPressEvent(String target, String action) {
    this(target, action, null);
  }

  public void setKeyEvent(KeyEvent keyEvent) {
    this.keyEvent = keyEvent;
  }

  public KeyPressEvent(KeyEvent event) {
    this.keyEvent = event;
  }

  public KeyPressEvent(String target, String action, Object[] arguments) {

    this.target = target;
    this.action = action;
    this.arguments = arguments;
  }

  /**
   * 'spec' has the form $target.action target is either a number or a hierarchical string of ids, e.g.
   * $MyFrame.Ok
   */
  public KeyPressEvent(String spec, Object[] args) {

    int i = spec.lastIndexOf('.');
    if (i > 0) {
      target = spec.substring(0, i);
      spec = spec.substring(i + 1);
    }

    action = spec;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String s) {
    target = s;
  }

  public String getAction() {
    return action;
  }

  public void setAction(String s) {
    action = s;
  }

  public Object[] getArguments() {
    return arguments;
  }

  public void addArgument(Object o) {

    if (arguments == null) {
      arguments = new Object[1];
      arguments[0] = o;
    } else {
      Object[] newArgs = new Object[arguments.length + 1];
      System.arraycopy(arguments, 0, newArgs, 0, arguments.length);
      newArgs[arguments.length] = o;
      arguments = newArgs;
    }

  }

  @Override
  public String toString() {
    return print();
  }

  @Override
  public String print() {
    StringBuilder b = new StringBuilder();

    if (target != null) {
      b.append(target);
      b.append('.');
    }
    if (action != null) {
      b.append(action);
      b.append('(');
      b.append(Arrays.toString(arguments));
      if (keyEvent != null && !action.equals("back")) {
        b.append(" KeyEvent [ action=" + KeyEvent.actionToString(keyEvent.getAction()) + " keyCode="
            + KeyEvent.keyCodeToString(keyEvent.getKeyCode()) + "]");
      }
    }
    b.append(')');
    return b.toString();
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    KeyEvent i = keyEvent.copy();
    KeyPressEvent event = new KeyPressEvent(i);
    event.action = this.action;
    event.target = this.target;
    event.arguments = new Object[this.arguments.length];
    System.arraycopy(this.arguments, 0, event.arguments, 0, this.arguments.length);
    return event;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((action == null) ? 0 : action.hashCode());
    result = prime * result + ((arguments == null || arguments.length == 0) ? 0 : Arrays.hashCode(arguments));
    result = prime * result + (((keyEvent == null) ? 0 : (keyEvent.getAction() + "." + keyEvent.getKeyCode()).hashCode()));
    result = prime * result + ((target == null) ? 0 : target.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof KeyPressEvent)) {
      return false;
    }
    KeyPressEvent other = (KeyPressEvent) obj;
    if (action == null) {
      if (other.action != null) {
        return false;
      }
    } else if (!action.equals(other.action)) {
      return false;
    }
    if (arguments == null) {
      if (other.arguments != null) {
        return false;
      }
    } else if (Arrays.equals(arguments, other.arguments)) {
      return false;
    }
    if (keyEvent == null) {
      if (other.keyEvent != null) {
        return false;
      }
    } else if (other.keyEvent == null || keyEvent.getKeyCode() != other.keyEvent.getKeyCode() || keyEvent.getAction() != other.keyEvent.getAction()) {
      return false;
    }
    if (target == null) {
      if (other.target != null) {
        return false;
      }
    } else if (!target.equals(other.target)) {
      return false;
    }
    return true;
  }

  public KeyEvent getKeyEvent() {
    return keyEvent;
  }

}
