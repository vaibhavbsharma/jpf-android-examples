package oauth.signpost.commonshttp;

import gov.nasa.jpf.vm.Abstraction;
import oauth.signpost.OAuthConsumer;

public class CommonsHttpOAuthProvider {
  public static oauth.signpost.commonshttp.CommonsHttpOAuthProvider TOP = new oauth.signpost.commonshttp.CommonsHttpOAuthProvider();

  public CommonsHttpOAuthProvider() {
  }

  public CommonsHttpOAuthProvider(java.lang.String param0, java.lang.String param1, java.lang.String param2) {
  }

  public String retrieveRequestToken(OAuthConsumer param0, String param1) {
    return Abstraction.TOP_STRING;
  }
}