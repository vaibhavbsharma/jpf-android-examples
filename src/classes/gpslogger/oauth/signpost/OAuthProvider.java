package oauth.signpost;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public interface OAuthProvider {


  public abstract void retrieveAccessToken(oauth.signpost.OAuthConsumer param0, java.lang.String param1) throws oauth.signpost.exception.OAuthMessageSignerException ,oauth.signpost.exception.OAuthNotAuthorizedException ,oauth.signpost.exception.OAuthExpectationFailedException ,oauth.signpost.exception.OAuthCommunicationException ;

  public abstract java.lang.String retrieveRequestToken(oauth.signpost.OAuthConsumer param0, java.lang.String param1) throws oauth.signpost.exception.OAuthMessageSignerException ,oauth.signpost.exception.OAuthNotAuthorizedException ,oauth.signpost.exception.OAuthExpectationFailedException ,oauth.signpost.exception.OAuthCommunicationException ;
}