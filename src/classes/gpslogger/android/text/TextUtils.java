package android.text;

public class TextUtils {
  public static final android.text.TextUtils TOP = new android.text.TextUtils();


  public TextUtils(){
  }

  public static boolean isEmpty(java.lang.CharSequence param0){
    return param0.length() == 0;
  }
}