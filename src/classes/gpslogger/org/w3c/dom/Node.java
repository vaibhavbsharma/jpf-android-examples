package org.w3c.dom;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public interface Node {


  public abstract org.w3c.dom.Node getFirstChild();

  public abstract org.w3c.dom.Node appendChild(org.w3c.dom.Node param0) throws org.w3c.dom.DOMException ;

  public abstract java.lang.String getNodeValue() throws org.w3c.dom.DOMException ;

  public abstract org.w3c.dom.Node removeChild(org.w3c.dom.Node param0) throws org.w3c.dom.DOMException ;

  public abstract short getNodeType();

  public abstract org.w3c.dom.NamedNodeMap getAttributes();

  public abstract java.lang.String getNodeName();

  public abstract org.w3c.dom.NodeList getChildNodes();
}