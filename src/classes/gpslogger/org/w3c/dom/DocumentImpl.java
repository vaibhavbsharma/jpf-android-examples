package org.w3c.dom;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class DocumentImpl implements org.w3c.dom.Document {
  public static org.w3c.dom.DocumentImpl TOP = new org.w3c.dom.DocumentImpl();


  public org.w3c.dom.NodeList getElementsByTagName(java.lang.String param0){
    return ((org.w3c.dom.NodeList)Abstraction.randomObject("org.w3c.dom.NodeList"));
  }

  public org.w3c.dom.Text createTextNode(java.lang.String param0){
    return ((org.w3c.dom.Text)Abstraction.randomObject("org.w3c.dom.Text"));
  }

  public org.w3c.dom.Element createElement(java.lang.String param0){
    return ((org.w3c.dom.Element)Abstraction.randomObject("org.w3c.dom.Element"));
  }

  public org.w3c.dom.Attr createAttribute(java.lang.String param0){
    return ((org.w3c.dom.Attr)Abstraction.randomObject("org.w3c.dom.Attr"));
  }

  public org.w3c.dom.Node getFirstChild(){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public org.w3c.dom.Node appendChild(org.w3c.dom.Node param0){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public java.lang.String getNodeValue(){
    return Abstraction.TOP_STRING;
  }

  public org.w3c.dom.Node removeChild(org.w3c.dom.Node param0){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public short getNodeType(){
    return Abstraction.TOP_SHORT;
  }

  public org.w3c.dom.NamedNodeMap getAttributes(){
    return ((org.w3c.dom.NamedNodeMap)Abstraction.randomObject("org.w3c.dom.NamedNodeMap"));
  }

  public java.lang.String getNodeName(){
    return Abstraction.TOP_STRING;
  }

  public org.w3c.dom.NodeList getChildNodes(){
    return ((org.w3c.dom.NodeList)Abstraction.randomObject("org.w3c.dom.NodeList"));
  }
}