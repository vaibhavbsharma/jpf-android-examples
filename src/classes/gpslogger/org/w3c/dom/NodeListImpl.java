package org.w3c.dom;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class NodeListImpl implements org.w3c.dom.NodeList {
  public static org.w3c.dom.NodeListImpl TOP = new org.w3c.dom.NodeListImpl();


  public org.w3c.dom.Node item(int param0){
    return ((org.w3c.dom.Node)Abstraction.randomObject("org.w3c.dom.Node"));
  }

  public int getLength(){
    return Abstraction.TOP_INT;
  }
}