package javax.mail;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public interface Part {


  public abstract void setText(java.lang.String param0) throws javax.mail.MessagingException ;

  public abstract void setDataHandler(javax.activation.DataHandler param0) throws javax.mail.MessagingException ;

  public abstract void setFileName(java.lang.String param0) throws javax.mail.MessagingException ;
}