package android.text.method;

public class PasswordTransformationMethod implements android.text.method.TransformationMethod {
  public static android.text.method.PasswordTransformationMethod TOP = new android.text.method.PasswordTransformationMethod();


  public PasswordTransformationMethod(){
  }

  public static android.text.method.PasswordTransformationMethod getInstance(){
    return android.text.method.PasswordTransformationMethod.TOP;
  }
}