package android.database.sqlite;

import gov.nasa.jpf.vm.AndroidVerify;

public class SQLiteDatabase extends SQLiteClosable {

  public static class CursorFactory {

  }

  public SQLiteDatabase() {
  }
  

  int mVersion = 1;

  public int getVersion() {
    return mVersion;

  }

  public void setVersion(int version) {
    mVersion = version;
  }


  public boolean isOpen() {
    return true;//AndroidVerify.getBoolean("SQLiteDatabase.isOpen()");
  }

  public android.database.Cursor query(boolean param0, java.lang.String param1, java.lang.String[] param2,
                                       java.lang.String param3, java.lang.String[] param4,
                                       java.lang.String param5, java.lang.String param6,
                                       java.lang.String param7, java.lang.String param8) {
    return new android.database.CursorImpl();
  }

  public int update(java.lang.String param0, android.content.ContentValues param1, java.lang.String param2,
                    java.lang.String[] param3) {
    return 1;
  }

  public long insert(java.lang.String param0, java.lang.String param1, android.content.ContentValues param2) {
    return 1;
  }

  public android.database.Cursor query(java.lang.String param0, java.lang.String[] param1,
                                       java.lang.String param2, java.lang.String[] param3,
                                       java.lang.String param4, java.lang.String param5,
                                       java.lang.String param6) {
    return new android.database.CursorImpl();
  }

  public void execSQL(java.lang.String param0) throws android.database.SQLException {
  }

  public int delete(java.lang.String param0, java.lang.String param1, java.lang.String[] param2) {
    // boolean b3 = AndroidVerify.getBoolean("SQLiteDatabase.delete");
    // if (!b3) {
    // throw new Exception("Can not read from DB");
    // }
    return 1;
  }

  public android.database.Cursor query(java.lang.String param0, java.lang.String[] param1,
                                       java.lang.String param2, java.lang.String[] param3,
                                       java.lang.String param4, java.lang.String param5,
                                       java.lang.String param6, java.lang.String param7) {
    return new android.database.CursorImpl();
  }
}