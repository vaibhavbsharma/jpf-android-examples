package android.database;

import gov.nasa.jpf.vm.AndroidVerify;

public class MergeCursor extends DefaultCursor {

  public MergeCursor() {
  }

  public MergeCursor(android.database.Cursor[] param0) {
  }

  public int getColumnIndexOrThrow(java.lang.String param0) throws java.lang.IllegalArgumentException {
    int var = 0;
    if (param0.equals("_id")) {
      var = 0;
    }
    if (param0.equals("artist")) {
      var = 3;
    }
    if (param0.equals("album")) {
      var = 4;
    }
    if (param0.equals("title")) {
      var = 2;
    }
    if (param0.equals("is_ringtone")) {
      var = 5;
    }
    if (param0.equals("_data")) {
      var = 1;
    }
    if (param0.equals("is_alarm")) {
      var = 6;
    }
    if (param0.equals("is_notification")) {
      var = 7;
    }
    if (param0.equals("is_music")) {
      var = 8;
    }
    return var;
  }

  public long getLong(int param0) {
    long var = 0;
    if (param0 == 0) {
      var = 19;
    }
    return var;
  }

  public java.lang.String getString(int param0) {
    String var = "";
    if (param0 == 3) {
      var = AndroidVerify.getString(new String[] { "Ringdroid", "The XX", "<unknown>" },
          "android.database.MergeCursor.getString(3)");
    }
    if (param0 == 4) {
      var = AndroidVerify.getString(new String[] { "<unknown>", "Coexist", "music", "ringtones" },
          "android.database.MergeCursor.getString(4)");
    }
    if (param0 == 2) {
      var = AndroidVerify.getString(new String[] {
          // "20160812062841Ringtone Ringtone11",
          // "20160812062841Ringtone Ringtone",
          // "20160812062841Ringtone Music",
          // "2016-08-12 06:28:41 Ringtone",
          "bensound-cute-cut", "Cute",
          // "20160812062841RingtoneRingtone",
          // "20160812062841RingtoneMusic1",
          // "20160812062841RingtoneMusic",
          "20160812062841Ringtone" }, "android.database.MergeCursor.getString(2)");
    }
    if (param0 == 1) {
      var = AndroidVerify.getString(new String[] {
          // "/sdcard/media/audio/ringtones/20160812062841RingtoneRingtone11.3gpp",
          // "/sdcard/media/audio/ringtones/20160812062841RingtoneRingtone.3gpp",
          // "/sdcard/media/audio/music/20160812062841RingtoneMusic1.3gpp",
          // "/sdcard/media/audio/music/20160812062841RingtoneMusic.3gpp",
          // "/sdcard/media/audio/ringtones/20160812062841Ringtone.3gpp",
          "/storage/sdcard/media/audio/ringtones/bensound-cute-cut.mp3", "/storage/sdcard/Test.mp3",
          // "/storage/sdcard/media/audio/ringtones/20160812062841RingtoneRingtone.3gpp",
          // "/storage/sdcard/media/audio/music/20160812062841RingtoneMusic1.3gpp",
          // "/storage/sdcard/media/audio/music/20160812062841RingtoneMusic.3gpp",
          "/storage/sdcard/media/audio/ringtones/20160812062841Ringtone.3gpp" },
          "android.database.MergeCursor.getString(1)");
    }
    return var;

  }

  public int getInt(int param0) {
    int var = -1;
    if (param0 == 5) {
      var = (int) AndroidVerify.getValues(new Object[]{-1,0}, "android.database.MergeCursor.getInt(5)");
    }
    if (param0 == 6) {
      var = (int) AndroidVerify.getValues(new Object[]{-1,0}, "android.database.MergeCursor.getInt(6)");
    }
    if (param0 == 7) {
      var =  (int) AndroidVerify.getValues(new Object[]{-1, 0}, "android.database.MergeCursor.getInt(7)");
    }
    if (param0 == 8) {
      var = (int) AndroidVerify.getValues(new Object[]{-1,0}, "android.database.MergeCursor.getInt(8)");
    }
    return var;

  }

}