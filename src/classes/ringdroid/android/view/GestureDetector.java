package android.view;

import gov.nasa.jpf.vm.Abstraction;

public class GestureDetector {
  public class OnGestureListener {

  }

  public static android.view.GestureDetector TOP = new android.view.GestureDetector();


  public GestureDetector(){
  }

  public GestureDetector(android.content.Context param0, android.view.GestureDetector.OnGestureListener param1){
  }

  public boolean onTouchEvent(android.view.MotionEvent param0){
    return Abstraction.TOP_BOOL;
  }
}