package org.apache.http.impl.client;

import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseImpl;
import org.apache.http.client.methods.HttpUriRequest;

public class DefaultHttpClient {
  public static org.apache.http.impl.client.DefaultHttpClient TOP = new org.apache.http.impl.client.DefaultHttpClient();

  public DefaultHttpClient() {
  }

  public DefaultHttpClient(org.apache.http.params.HttpParams param0) {
  }

  public HttpResponse execute(HttpUriRequest param0) {
    return HttpResponseImpl.TOP;
  }

}