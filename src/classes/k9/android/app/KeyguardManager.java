package android.app;
import gov.nasa.jpf.vm.Abstraction;

public class KeyguardManager {
  public static android.app.KeyguardManager TOP = new android.app.KeyguardManager();


  public KeyguardManager(){
  }

  public boolean inKeyguardRestrictedInputMode(){
    return Abstraction.TOP_BOOL;
  }
}