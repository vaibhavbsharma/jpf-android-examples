package android.app;

import gov.nasa.jpf.vm.Abstraction;

public class ExpandableListActivity extends android.app.Activity {

  public ExpandableListActivity() {
  }

  public android.widget.ExpandableListView getExpandableListView() {
    return ((android.widget.ExpandableListView) Abstraction.randomObject("android.widget.ExpandableListView"));
  }

  public void setListAdapter(android.widget.ExpandableListAdapter param0) {
  }
}