package android.view;

import gov.nasa.jpf.vm.Abstraction;

public class GestureDetector {
  public static android.view.GestureDetector TOP = new android.view.GestureDetector();

  public static class OnGestureListener {
  }

  public GestureDetector() {
  }

  public GestureDetector(android.view.GestureDetector.OnGestureListener param0) {
  }

  public boolean onTouchEvent(android.view.MotionEvent param0) {
    return Abstraction.TOP_BOOL;
  }
}