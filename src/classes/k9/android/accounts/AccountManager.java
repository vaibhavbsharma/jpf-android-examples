package android.accounts;
import gov.nasa.jpf.vm.Abstraction;

public class AccountManager {
  public static android.accounts.AccountManager TOP = new android.accounts.AccountManager();


  public AccountManager(){
  }

  public static android.accounts.AccountManager get(android.content.Context param0){
    return ((android.accounts.AccountManager)Abstraction.randomObject("android.accounts.AccountManager"));
  }

  public android.accounts.Account[] getAccounts(){
    return ((android.accounts.Account[])Abstraction.randomObject("android.accounts.Account[]"));
  }
}