package org.apache.james.mime4j.dom.address;
import gov.nasa.jpf.vm.Abstraction;

public class Mailbox {
  public static org.apache.james.mime4j.dom.address.Mailbox TOP = new org.apache.james.mime4j.dom.address.Mailbox();


  public Mailbox(){
  }

  public java.lang.String getLocalPart(){
    return Abstraction.TOP_STRING;
  }

  public java.lang.String getDomain(){
    return Abstraction.TOP_STRING;
  }

  public java.lang.String getName(){
    return Abstraction.TOP_STRING;
  }
}