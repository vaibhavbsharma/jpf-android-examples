package org.apache.http;
import gov.nasa.jpf.vm.Abstraction;

public class StatusLineImpl implements org.apache.http.StatusLine {
  public static org.apache.http.StatusLineImpl TOP = new org.apache.http.StatusLineImpl();


  public int getStatusCode(){
    return Abstraction.TOP_INT;
  }
}