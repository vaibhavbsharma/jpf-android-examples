package org.apache.http;

public interface HttpResponse extends org.apache.http.HttpMessage {


  public abstract org.apache.http.StatusLine getStatusLine();

  public abstract org.apache.http.HttpEntity getEntity();

  public abstract org.apache.http.Header getFirstHeader(java.lang.String param0);
}