package org.apache.http;

public interface StatusLine {


  public abstract int getStatusCode();
}