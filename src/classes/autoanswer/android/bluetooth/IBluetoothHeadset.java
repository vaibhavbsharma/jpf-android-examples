package android.bluetooth;

import android.os.IBinder;

public interface IBluetoothHeadset extends IBinder {

  public static class Stub {
    public static android.bluetooth.IBluetoothHeadset asInterface(android.os.IBinder obj) {
      return (IBluetoothHeadset) obj;

    }
  }

  public abstract int getState() throws android.os.RemoteException;
}