package com.udojava.evalex;

import gov.nasa.jpf.vm.AndroidVerify;

public class Expression {

  public Expression() {
  }

  public Expression(java.lang.String param0) {
  }

  public java.math.BigDecimal eval() {
    boolean b = AndroidVerify.getBoolean("Expression.eval()");
    if (!b) {
      return java.math.BigDecimal.TOP;
    } else {
      throw new RuntimeException("Error evaluation expression");
    }

  }
}